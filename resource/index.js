var myFavTechnologies=angular.module("myFavTechnologies", []);


myFavTechnologies.controller("MyFavTechnologiesController",["$scope", function($scope){

    $scope.message = "Fav Technologies are :";
    $scope.favTechnologies = [
        {
            name: "java",
            version: "1.8",
            cost: 500
        },
        {
            name: "ReactJs",
            version: "1.7",
            cost: 600
        },
        {
            name: "AngularJs",
            version: "1.6",
            cost: 700
        },
        {
            name: "NodeJs",
            version: "1.5",
            cost: 400
        }

    ]
    
}]);